﻿using System.Runtime.Serialization;

namespace PanMainAPI
{
    /// <summary>
    /// This is the object used to return details to the applications to display to the users.
    /// </summary>
    [DataContract]
    public class ReturnMessage
    {
        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Description { get; set; }

        public ReturnMessage(string code, string msg)
        {
            Code = code;
            Message = msg;
        }

        public ReturnMessage(string code, string msg, string desc)
        {
            Code = code;
            Message = msg;
            Description = desc;
        }
    }

    /// <summary>
    /// This is the object used to get the updates needed for the DB.
    /// </summary>
    [DataContract]
    public class DBVersion
    {
        [DataMember]
        public string UserID { get; set; }

        [DataMember]
        public int Major { get; set; }

        [DataMember]
        public int Minor { get; set; }

        [DataMember]
        public int Build { get; set; }

        [DataMember]
        public int Revision { get; set; }

        public DBVersion(string userid, int major, int minor, int build, int revision)
        {
            UserID = userid;
            Major = major;
            Minor = minor;
            Build = build;
            Revision = revision;
        }

        public DBVersion()
        {
            UserID = "";
            Major = 0;
            Minor = 0;
            Build = 0;
            Revision = 0;
        }
    }

    /// <summary>
    /// This is the object used to manage the code tables from the DB.
    /// </summary>
    [DataContract]
    public class CodeTableValue
    {
        [DataMember]
        public string CodeTable { get; set; }

        [DataMember]
        public bool Active { get; set; }

        [DataMember]
        public bool SponsorshipSite { get; set; }

        [DataMember]
        public int CodeID { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string PublicDescription { get; set; }

        [DataMember]
        // Also used for Required. 
        public int MinAge { get; set; }

        [DataMember]
        public int ParentCodeID { get; set; }

        public CodeTableValue(string codetable)
        {
            CodeTable = codetable;
            Active = true;
            SponsorshipSite = true;
            CodeID = 0;
            Code = "";
            Description = "";
            PublicDescription = "";
            MinAge = 0;
            ParentCodeID = 0;
        }

        public CodeTableValue(string codetable, string code, string description, string publicdescription)
        {
            CodeTable = codetable;
            Active = true;
            SponsorshipSite = true;
            CodeID = 0;
            Code = code;
            Description = description;
            PublicDescription = publicdescription;
            MinAge = 0;
            ParentCodeID = 0;
        }

        public CodeTableValue(string codetable, int codeid, string code, string description, string publicdescription)
        {
            CodeTable = codetable;
            Active = true;
            SponsorshipSite = true;
            CodeID = codeid;
            Code = code;
            Description = description;
            PublicDescription = publicdescription;
            MinAge = 0;
            ParentCodeID = 0;
        }

        public CodeTableValue(string codetable, bool active, int codeid)
        {
            CodeTable = codetable;
            Active = active;
            SponsorshipSite = true;
            CodeID = codeid;
            Code = "";
            Description = "";
            PublicDescription = "";
            MinAge = 0;
            ParentCodeID = 0;
        }


    }
}