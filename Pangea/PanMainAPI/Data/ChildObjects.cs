﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PanMainAPI
{
    /// <summary>
    /// This is the object used to contain and collect the data needed for a child for admin purposes.
    /// </summary>
    [DataContract]
    public class AdminChild
    {
        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public string ChildNum { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        public int GradeLevelCodeID { get; set; }

        [DataMember]
        public string OtherNameGoesBy { get; set; }

        [DataMember]
        public int HealthStatusCodeID { get; set; }

        [DataMember]
        public int LivesWithCodeID { get; set; }

        [DataMember]
        public int FavoriteLearningCodeID { get; set; }

        [DataMember]
        public int GenderCodeID { get; set; }

        [DataMember]
        public int NumberOfBrothers { get; set; }

        [DataMember]
        public int NumberOfSisters { get; set; }

        [DataMember]
        public bool DisabilityStatus { get; set; }

        [DataMember]
        public int LocationCodeID { get; set; }

        [DataMember]
        public int PersonalityTypeID { get; set; }

        [DataMember]
        public string MajorLifeEvent { get; set; }

        [DataMember]
        public int LanguageCodeID { get; set; }

        [DataMember]
        public List<int> ChoreIDs { get; set; }

        [DataMember]
        public List<int> FavoriteActivitieIDs { get; set; }

        [DataMember]
        public List<string> ConfirmedNonDups { get; set; }

        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public string ActionType { get; set; }

        [DataMember]
        public string ActionReason { get; set; }

        public AdminChild()
        {
            FavoriteActivitieIDs = new List<int>();
            ChoreIDs = new List<int>();
            ConfirmedNonDups = new List<string>();
        }

        public AdminChild(string chldId)
        {
            ChildID = chldId;
            FavoriteActivitieIDs = new List<int>();
            ChoreIDs = new List<int>();
            ConfirmedNonDups = new List<string>();
        }
    }

    /// <summary>
    /// This is the object used to contain and collect the data needed for enrolling a child.
    /// </summary>
    [DataContract]
    public class EnrollChild
    {
        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime DateOfBirth { get; set; }

        [DataMember]
        public int GradeLevelCodeID { get; set; }

        [DataMember]
        public string OtherNameGoesBy { get; set; }

        [DataMember]
        public int HealthStatusCodeID { get; set; }

        [DataMember]
        public int LivesWithCodeID { get; set; }

        [DataMember]
        public int FavoriteLearningCodeID { get; set; }

        [DataMember]
        public int GenderCodeID { get; set; }

        [DataMember]
        public int NumberOfBrothers { get; set; }

        [DataMember]
        public int NumberOfSisters { get; set; }

        [DataMember]
        public bool DisabilityStatus { get; set; }

        [DataMember]
        public int LocationCodeID { get; set; }

        [DataMember]
        public int PersonalityTypeID { get; set; }

        [DataMember]
        public List<int> ChoreIDs { get; set; }

        [DataMember]
        public List<int> FavoriteActivitieIDs { get; set; }

        [DataMember(IsRequired = true)]
        public List<string> ConfirmedNonDups { get; set; }

        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public string ActionType { get; set; }

        [DataMember]
        public string ActionReason { get; set; }

        public EnrollChild()
        {
            FavoriteActivitieIDs = new List<int>();
            ChoreIDs = new List<int>();
            ConfirmedNonDups = new List<string>();
        }

        public EnrollChild(string chldId)
        {
            ChildID = chldId;
            FavoriteActivitieIDs = new List<int>();
            ChoreIDs = new List<int>();
            ConfirmedNonDups = new List<string>();
        }
    }

    /// <summary>
    /// This is the object used to contain and collect the data needed for updating a child.
    /// </summary>
    [DataContract]
    public class UpdateChild
    {
        [DataMember]
        public string ChildID { get; set; }

        [DataMember]
        public string ChildNum { get; set; }

        [DataMember]
        public string OtherNameGoesBy { get; set; }

        [DataMember]
        public int GradeLevelCodeID { get; set; }

        [DataMember]
        public int HealthStatusCodeID { get; set; }

        [DataMember]
        public int LivesWithCodeID { get; set; }

        [DataMember]
        public int FavoriteLearningCodeID { get; set; }

        [DataMember]
        public int NumberOfBrothers { get; set; }

        [DataMember]
        public int NumberOfSisters { get; set; }

        [DataMember]
        public bool DisabilityStatus { get; set; }

        [DataMember]
        public int PersonalityTypeID { get; set; }

        [DataMember]
        public List<int> ChoreIDs { get; set; }

        [DataMember]
        public List<int> FavoriteActivitieIDs { get; set; }

        [DataMember]
        public List<string> ConfirmedNonDups { get; set; }

        [DataMember]
        public string MajorLifeEvent { get; set; }

        [DataMember]
        public int LanguageCodeID { get; set; }

        [DataMember]
        public int LocationCodeID { get; set; }

        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public string ActionType { get; set; }

        [DataMember]
        public string ActionReason { get; set; }

        public UpdateChild()
        {
            FavoriteActivitieIDs = new List<int>();
            ChoreIDs = new List<int>();
            ConfirmedNonDups = new List<string>();
        }

        public UpdateChild(string chldId)
        {
            ChildID = chldId;
            FavoriteActivitieIDs = new List<int>();
            ChoreIDs = new List<int>();
            ConfirmedNonDups = new List<string>();
        }
    }
}