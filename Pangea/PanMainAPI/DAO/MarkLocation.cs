﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace PanMainAPI.DAO
{
    public class MarkLocation
    {
        string sqlConnection = ConfigurationManager.ConnectionStrings["PangeaConnection"].ConnectionString;

        public ReturnMessage MarkLocation4Update(string userid, string locationid, DateTime visitdate, int leaddays, DateTime duedate)
        {
            ReturnMessage ret = new ReturnMessage("0", "Success", "Mark Location for Update successful.");
            SqlConnection Con = new SqlConnection(sqlConnection);

            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Mark_Location_for_Update]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Location_Code_ID", locationid);
                cmd.Parameters.AddWithValue("@Visit_Date", visitdate);
                if (leaddays > 0)
                    cmd.Parameters.AddWithValue("@Lead_Days", leaddays);
                cmd.Parameters.AddWithValue("@Due_Date", duedate);
                Con.Open();
                cmd.ExecuteNonQuery();

                //if (leaddays == 0)
                //{
                //    cmd.Parameters.Clear();

                //    cmd = new SqlCommand("[dbo].[usp_Mark_Location_for_Update]", Con);
                //    cmd.CommandType = CommandType.StoredProcedure;
                //    cmd.Parameters.AddWithValue("@User_ID", userid);
                //    cmd.ExecuteNonQuery();
                //}
            }
            catch (Exception ex)
            {
                ret.Code = "-1";
                ret.Message = "Error";
                ret.Description = "Mark Location for Update error:" + ex.Message;
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return ret;
        }

        public List<MarkLocationForUpdateValue> GetLocation4Update(string userid, string locationid, string countryid)
        {
            List<MarkLocationForUpdateValue> _retList = new List<MarkLocationForUpdateValue>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet _dataSet = new DataSet();

            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Location_for_Update]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                if (!String.IsNullOrEmpty(locationid))
                    cmd.Parameters.AddWithValue("@Location_Code_ID", locationid);
                if (!String.IsNullOrEmpty(countryid))
                    cmd.Parameters.AddWithValue("@Country_Code_ID", countryid);

                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(_dataSet);

                foreach (DataRow row in _dataSet.Tables[0].Rows)
                {
                    MarkLocationForUpdateValue _markLocValue = new MarkLocationForUpdateValue();

                    if (!string.IsNullOrEmpty(row["Region_Code_ID"].ToString()))
                        _markLocValue.RegionCode = row["Region_Code_ID"].ToString();
                    if (!string.IsNullOrEmpty(row["Region_Name"].ToString()))
                        _markLocValue.RegionName = row["Region_Name"].ToString();
                    if (!string.IsNullOrEmpty(row["Country_Code_ID"].ToString()))
                        _markLocValue.CountryCodeID = row["Country_Code_ID"].ToString();
                    if (!string.IsNullOrEmpty(row["Country_Name"].ToString()))
                        _markLocValue.CountryName = row["Country_Name"].ToString();
                    if (!string.IsNullOrEmpty(row["Location_Code_ID"].ToString()))
                        _markLocValue.LocationCodeID = row["Location_Code_ID"].ToString();
                    if (!string.IsNullOrEmpty(row["Location_Name"].ToString()))
                        _markLocValue.LocationName = row["Location_Name"].ToString();
                    if (!string.IsNullOrEmpty(row["Visit_Date"].ToString()))
                        _markLocValue.VisitDate = DateTime.Parse(row["Visit_Date"].ToString());
                    if (!string.IsNullOrEmpty(row["Lead_Days"].ToString()))
                        _markLocValue.LeadDays = row["Lead_Days"].ToString();
                    if (!string.IsNullOrEmpty(row["Due_Date"].ToString()))
                        _markLocValue.DueDate = DateTime.Parse(row["Due_Date"].ToString());
                    if (!string.IsNullOrEmpty(row["Location_Calendar_ID"].ToString()))
                        _markLocValue.LocationCalendarID = row["Location_Calendar_ID"].ToString();

                    _retList.Add(_markLocValue);
                }
            }
            catch (Exception ex)
            {
                _retList = null;
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return _retList;
        }

        public ReturnMessage CancelLocation4Update(string userid, string locationcalid)
        {
            ReturnMessage ret = new ReturnMessage("0", "Success", "Mark Location for Update successful.");
            SqlConnection Con = new SqlConnection(sqlConnection);

            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Cancel_Location_for_Update]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Location_Calendar_ID", locationcalid);
                Con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ret.Code = "-1";
                ret.Message = "Error";
                ret.Description = "Mark Location for Update error:" + ex.Message;
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return ret;
        }

        public ReturnMessage EditLocation4Update(string userid, string locationid, int locationCalendarID, DateTime visitdate, int leaddays, DateTime duedate)
        {
            ReturnMessage ret = new ReturnMessage("0", "Success", "Edit Mark Location for Update successful.");
            SqlConnection Con = new SqlConnection(sqlConnection);

            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Mark_Location_for_Update]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                cmd.Parameters.AddWithValue("@Location_Code_ID", locationid);
                cmd.Parameters.AddWithValue("@Location_Calendar_ID", locationCalendarID);
                cmd.Parameters.AddWithValue("@Visit_Date", visitdate);
                if (leaddays > 0)
                    cmd.Parameters.AddWithValue("@Lead_Days", leaddays);
                cmd.Parameters.AddWithValue("@Due_Date", duedate);
                Con.Open();
                cmd.ExecuteNonQuery();

                //if (leaddays == 0)
                //{
                //    cmd.Parameters.Clear();

                //    cmd = new SqlCommand("[dbo].[usp_Mark_Location_for_Update]", Con);
                //    cmd.CommandType = CommandType.StoredProcedure;
                //    cmd.Parameters.AddWithValue("@User_ID", userid);
                //    cmd.ExecuteNonQuery();
                //}
            }
            catch (Exception ex)
            {
                ret.Code = "-1";
                ret.Message = "Error";
                ret.Description = "Mark Location for Update error:" + ex.Message;
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return ret;
        }
    }
}