﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace PanMainAPI.DAO
{
    public class Workflow
    {
        string sqlConnection = ConfigurationManager.ConnectionStrings["PangeaConnection"].ConnectionString;


        public DataSet GetWorkFlows(int WorkFlowID, int CountryID, int LocationID, string UserID, string QCApprove)
        {
            SqlConnection Con = new SqlConnection(sqlConnection);

            DataSet OpenWorkFlows = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Child_Workflow_Summary]", Con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_ID", UserID);

                if (CountryID > 0)
                    cmd.Parameters.AddWithValue("@Country_Code_ID", CountryID);

                if (LocationID > 0)
                    cmd.Parameters.AddWithValue("@Location_Code_ID", LocationID);

                if (WorkFlowID > 0)
                    cmd.Parameters.AddWithValue("@Workflow_Code_ID", WorkFlowID);

                if (QCApprove == "true")
                {
                    cmd.Parameters.AddWithValue("@QC_Approve", true);
                }

                if (QCApprove == "false")
                {
                    cmd.Parameters.AddWithValue("@QC_Approve", false);
                }

                SqlDataAdapter adptr = new SqlDataAdapter();

                adptr.SelectCommand = cmd;

                Con.Open();

                adptr.Fill(OpenWorkFlows);
            }
            catch (Exception ex)
            {
                DataTable table = new DataTable("Errors");

                table.Columns.Add("Message");

                table.Rows.Add(table.NewRow());

                table.Rows[0][0] = "ERROR: " + ex.Message;

                OpenWorkFlows.Tables.Add(table);
            }
            finally
            {
                Con.Close();

                Con.Dispose();
            }

            return OpenWorkFlows;
        }

        public List<WorkFlowStep> GetChildWorkFlow(int ChildID, int WorkFlowID, string UserID, bool QCApprover)
        {
            List<WorkFlowStep> WorkFlowSteps = new List<WorkFlowStep>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataTable possibledup = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Child_Workflow_Step_Codes]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                cmd.Parameters.AddWithValue("@Pending_Child_ID", ChildID);
                cmd.Parameters.AddWithValue("@Workflow_Code_ID", WorkFlowID);
                cmd.Parameters.AddWithValue("@QC_Approve", QCApprover);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(possibledup);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            if (possibledup.Rows.Count > 0)
            {
                foreach (DataRow row in possibledup.Rows)
                {
                    WorkFlowStep Step = new WorkFlowStep();
                    Step.Pending_Child_ID = ChildID;
                    Step.Workflow_Code_ID = WorkFlowID;
                    if (!string.IsNullOrEmpty(row["Step_Code_ID"].ToString()))
                        Step.Step_Code_ID = int.Parse(row["Step_Code_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Step"].ToString()))
                        Step.Step_Name = row["Step"].ToString();
                    if (!string.IsNullOrEmpty(row["Child_Workflow_Step_ID"].ToString()))
                        Step.Child_Workflow_Step_ID = int.Parse(row["Child_Workflow_Step_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["File_ID"].ToString()))
                        Step.File_ID = int.Parse(row["File_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Order"].ToString()))
                        Step.Order = int.Parse(row["Order"].ToString());
                    if (!string.IsNullOrEmpty(row["SubOrder"].ToString()))
                        Step.SubOrder = int.Parse(row["SubOrder"].ToString());
                    if (!string.IsNullOrEmpty(row["Workflow_Response_to_Child_Workflow_Step"].ToString()))
                        Step.Workflow_Response_to_Step = int.Parse(row["Workflow_Response_to_Child_Workflow_Step"].ToString());
                    WorkFlowSteps.Add(Step);
                }
            }

            return WorkFlowSteps;
        }

        public ReturnMessage SaveWorkFlow(WorkFlowStep WorkFlow, string UserID)
        {
            ReturnMessage ret = new ReturnMessage("1", "Success");
            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Update_Child_Workflow_Step]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                if (WorkFlow.Pending_Child_ID > 0)
                    cmd.Parameters.AddWithValue("@Pending_Child_ID", WorkFlow.Pending_Child_ID);
                if (WorkFlow.Workflow_Code_ID > 0)
                    cmd.Parameters.AddWithValue("@Workflow_Code_ID", WorkFlow.Workflow_Code_ID);
                if (WorkFlow.Step_Code_ID > 0)
                    cmd.Parameters.AddWithValue("@Step_Code_ID", WorkFlow.Step_Code_ID);
                if (WorkFlow.Child_Workflow_Step_ID > 0)
                    cmd.Parameters.AddWithValue("@Child_Workflow_Step_ID", WorkFlow.Child_Workflow_Step_ID);
                if (WorkFlow.File_ID > 0)
                    cmd.Parameters.AddWithValue("@File_ID", WorkFlow.File_ID);
                if (WorkFlow.Status_Code_ID > 0)
                    cmd.Parameters.AddWithValue("@Status_Code_ID", WorkFlow.Status_Code_ID);
                if (WorkFlow.Order > 0)
                    cmd.Parameters.AddWithValue("@Order", WorkFlow.Order);
                if (WorkFlow.SubOrder > 0)
                    cmd.Parameters.AddWithValue("@SubOrder", WorkFlow.SubOrder);
                if (WorkFlow.Workflow_Response_to_Step > 0)
                    cmd.Parameters.AddWithValue("@Workflow_Response_to_Step", WorkFlow.Workflow_Response_to_Step);
                cmd.Parameters.AddWithValue("@QC_Approver", WorkFlow.QCApprover);
                cmd.Parameters.AddWithValue("@Complete", WorkFlow.Complete);
                Con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ret = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return ret;
        }

        public ReturnMessage AddSubWorkFlow(SubWorkFlow WorkFlow, string UserID)
        {
            ReturnMessage ret = new ReturnMessage("1", "Success");
            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Add_Child_SubWorkflow]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                if (WorkFlow.Pending_Child_ID > 0)
                    cmd.Parameters.AddWithValue("@Pending_Child_ID", WorkFlow.Pending_Child_ID);
                if (WorkFlow.Workflow_Code_ID > 0)
                    cmd.Parameters.AddWithValue("@Workflow_Code_ID", WorkFlow.Workflow_Code_ID);
                if (WorkFlow.Child_Workflow_Step_ID > 0)
                    cmd.Parameters.AddWithValue("@Child_Workflow_Step_ID", WorkFlow.Child_Workflow_Step_ID);
                if (WorkFlow.File_ID > 0)
                    cmd.Parameters.AddWithValue("@File_ID", WorkFlow.File_ID);
                Con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ret = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return ret;
        }

        public ReturnMessage DeclineStep(Decline Decline, string UserID)
        {
            ReturnMessage ret = new ReturnMessage("1", "Success");
            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Decline_Child_Workflow_Step]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                if (Decline.Child_Workflow_Step_ID > 0)
                    cmd.Parameters.AddWithValue("@Child_Workflow_Step_ID", Decline.Child_Workflow_Step_ID);
                if (Decline.Decline_Reason_Code > 0)
                    cmd.Parameters.AddWithValue("@Decline_Reason_Code_ID", Decline.Decline_Reason_Code);
                if (Decline.Decline_SubReason_Code > 0)
                    cmd.Parameters.AddWithValue("@Decline_SubReason_Code_ID", Decline.Decline_SubReason_Code);
                if (Decline.Workflow_Response > 0)
                    cmd.Parameters.AddWithValue("@Workflow_Response", Decline.Workflow_Response);
                cmd.Parameters.AddWithValue("@Decline_Notes", String.IsNullOrEmpty(Decline.Decline_Note) ? String.Empty : Decline.Decline_Note);

                Con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ret = new ReturnMessage("-1", "Error:" + ex.Message);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }
            return ret;
        }

        public List<ReturnMessage> DeclineUpdate(AdminChild child, string userid)
        {
            List<ReturnMessage> returnMessages = new List<ReturnMessage>();

            //Add db connection and procesing here. 
            SqlConnection Con = new SqlConnection(sqlConnection);
            try
            {
                SqlCommand cmd = new SqlCommand("[Pending].[usp_Add_Child]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", userid);
                if (!String.IsNullOrEmpty(child.ChildID))
                    cmd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                if (!String.IsNullOrEmpty(child.ChildNum))
                    cmd.Parameters.AddWithValue("@Child_Number", child.ChildNum);
                if (!String.IsNullOrEmpty(child.FirstName))
                    cmd.Parameters.AddWithValue("@First_Name", child.FirstName);
                if (!String.IsNullOrEmpty(child.LastName))
                    cmd.Parameters.AddWithValue("@Last_Name", child.LastName);
                if (!String.IsNullOrEmpty(child.MiddleName))
                    cmd.Parameters.AddWithValue("@Middle_Name", child.MiddleName);
                if (child.DateOfBirth != DateTime.MinValue)
                    cmd.Parameters.AddWithValue("@Date_of_Birth", child.DateOfBirth);
                if (child.GradeLevelCodeID > 0)
                    cmd.Parameters.AddWithValue("@Grade_Level_Code_ID", child.GradeLevelCodeID);
                if (child.HealthStatusCodeID > 0)
                    cmd.Parameters.AddWithValue("@Health_Status_Code_ID", child.HealthStatusCodeID);
                if (child.LivesWithCodeID > 0)
                    cmd.Parameters.AddWithValue("@Lives_With_Code_ID", child.LivesWithCodeID);
                if (child.FavoriteLearningCodeID > 0)
                    cmd.Parameters.AddWithValue("@Favorite_Learning_Code_ID", child.FavoriteLearningCodeID);
                //cmd.Parameters.AddWithValue("@Child_Record_Status_Code_ID", );
                //cmd.Parameters.AddWithValue("@Child_Remove_Reason_Code_ID", );
                if (child.GenderCodeID > 0)
                    cmd.Parameters.AddWithValue("@Gender_Code_ID", child.GenderCodeID);
                if (child.NumberOfBrothers > 0)
                    cmd.Parameters.AddWithValue("@Number_Brothers", child.NumberOfBrothers);
                if (child.NumberOfSisters > 0)
                    cmd.Parameters.AddWithValue("@Number_Sisters", child.NumberOfSisters);
                cmd.Parameters.AddWithValue("@Disability_Status", child.DisabilityStatus);
                if (child.LocationCodeID > 0)
                    cmd.Parameters.AddWithValue("@Location_Code_ID", child.LocationCodeID);
                if (!String.IsNullOrEmpty(child.OtherNameGoesBy))
                    cmd.Parameters.AddWithValue("@NickName", child.OtherNameGoesBy);
                Con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (string.IsNullOrEmpty(child.ChildID))
                        child.ChildID = reader["Child_ID"].ToString();
                    returnMessages.Add(new ReturnMessage(reader["Child_ID"].ToString(), "Child_ID", "Success! Decline update completed. All current progress has been saved."));
                }
                Con.Close();

                //try
                //{
                //    // Clear is called so that only the current codes are attached to the child
                //    SqlCommand cmnd = new SqlCommand("[Pending].[usp_Clear_Child_Codes]", Con);
                //    cmnd.CommandType = CommandType.StoredProcedure;
                //    cmnd.Parameters.AddWithValue("@User_ID", userid);
                //    cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                //    cmnd.Parameters.AddWithValue("@End_Date", DateTime.Now);
                //    Con.Open();
                //    cmnd.ExecuteNonQuery();
                //}
                //catch (Exception ex)
                //{
                //    returnMessages.Add(new ReturnMessage("-3", "ERROR", "DeclineUpdate failed: " + ex.Message));
                //}
                //finally
                //{
                //    Con.Close();
                //}

                if (child.PersonalityTypeID != 0)
                    try
                    {
                        SqlCommand cmnd = new SqlCommand("[Pending].[usp_Add_Child_Personality_Type]", Con);
                        cmnd.CommandType = CommandType.StoredProcedure;
                        cmnd.Parameters.AddWithValue("@User_ID", userid);
                        cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                        cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                        cmnd.Parameters.AddWithValue("@Personality_Type_Code_ID", child.PersonalityTypeID);
                        //cmnd.Parameters.AddWithValue("@End_Date", "");
                        Con.Open();
                        cmnd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        returnMessages.Add(new ReturnMessage("-4", "ERROR", "DeclineUpdate failed: " + ex.Message));
                    }
                    finally
                    {
                        Con.Close();
                    }

                if (child.FavoriteActivitieIDs != null)
                    if (child.FavoriteActivitieIDs.Count > 0)
                        foreach (int ActivityID in child.FavoriteActivitieIDs)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[Pending].[usp_Add_Child_Favorite_Activity]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                                cmnd.Parameters.AddWithValue("@Favorite_Activity_Code_ID", ActivityID);
                                //cmnd.Parameters.AddWithValue("@End_Date", "");
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-5", "ERROR", "DeclineUpdate failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }

                if (child.ChoreIDs != null)
                    if (child.ChoreIDs.Count > 0)
                    {
                        foreach (int Chore in child.ChoreIDs)
                        {
                            try
                            {
                                SqlCommand cmnd = new SqlCommand("[Pending].[usp_Add_Child_Chore]", Con);
                                cmnd.CommandType = CommandType.StoredProcedure;
                                cmnd.Parameters.AddWithValue("@User_ID", userid);
                                cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                                cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                                cmnd.Parameters.AddWithValue("@Chore_Code_ID", Chore);
                                //cmnd.Parameters.AddWithValue("@End_Date", "");
                                Con.Open();
                                cmnd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                returnMessages.Add(new ReturnMessage("-6", "ERROR", "DeclineUpdate failed: " + ex.Message));
                            }
                            finally
                            {
                                Con.Close();
                            }
                        }
                    }

                if (child.ConfirmedNonDups != null)
                    if (child.ConfirmedNonDups.Count > 0)
                    {
                        foreach (string childid in child.ConfirmedNonDups)
                        {
                            // call db add not dup
                        }
                    }

                if (!string.IsNullOrEmpty(child.MajorLifeEvent))
                {
                    try
                    {
                        SqlCommand cmnd = new SqlCommand("[Pending].[usp_Add_Child_Major_Life_Event]", Con);
                        cmnd.CommandType = CommandType.StoredProcedure;
                        cmnd.Parameters.AddWithValue("@User_ID", userid);
                        cmnd.Parameters.AddWithValue("@Child_ID", child.ChildID);
                        cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                        cmnd.Parameters.AddWithValue("@Language_Code_ID", child.LanguageCodeID);
                        cmnd.Parameters.AddWithValue("@Major_Life_Description", child.MajorLifeEvent);
                        //cmnd.Parameters.AddWithValue("@End_Date", "");
                        Con.Open();
                        cmnd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        returnMessages.Add(new ReturnMessage("-7", "ERROR", "DeclineUpdate failed: " + ex.Message));
                    }
                    finally
                    {
                        Con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
                returnMessages.Add(new ReturnMessage("-2", "ERROR", "DeclineUpdate failed: " + ex.Message));
            }

            // save non dup list

            return returnMessages;
        }

        public DataSet GetTranslationWorkFlows(int ActionReason, int CountryID, int LocationID, int LanguageID, string UserID)
        {
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet OpenWorkFlows = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Translation_Workflow_Summary]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                if (CountryID > 0)
                    cmd.Parameters.AddWithValue("@Country_Code_ID", CountryID);
                if (LocationID > 0)
                    cmd.Parameters.AddWithValue("@Location_Code_ID", LocationID);
                if (ActionReason > 0)
                    cmd.Parameters.AddWithValue("@Action_Reason_ID", ActionReason);
                if (LanguageID > 0)
                    cmd.Parameters.AddWithValue("@Language_Code_ID", LanguageID);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(OpenWorkFlows);
            }
            catch (Exception ex)
            {
                DataTable table = new DataTable("Errors");
                table.Columns.Add("Message");
                table.Rows.Add(table.NewRow());
                table.Rows[0][0] = "ERROR: " + ex.Message;
                OpenWorkFlows.Tables.Add(table);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return OpenWorkFlows;
        }

        public List<TranslationStep> GetChildTranslationWorkFlow(string ChildID, int ChildNumber, string UserID)
        {
            List<TranslationStep> TranslationSteps = new List<TranslationStep>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataTable possibledup = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Child_Translations]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                if (ChildNumber > 0)
                    cmd.Parameters.AddWithValue("@Child_Number", ChildNumber);
                if (!string.IsNullOrEmpty(ChildID))
                    cmd.Parameters.AddWithValue("@Child_ID", ChildID);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(possibledup);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            if (possibledup.Rows.Count <= 0) return TranslationSteps;
            foreach (DataRow row in possibledup.Rows)
            {
                TranslationStep Step = new TranslationStep();
                if (!string.IsNullOrEmpty(row["Child_Workflow_Step_ID"].ToString()))
                    Step.WorkflowStepID = int.Parse(row["Child_Workflow_Step_ID"].ToString());
                if (!string.IsNullOrEmpty(row["First_Name"].ToString()))
                    Step.FirstName = row["First_Name"].ToString();
                if (!string.IsNullOrEmpty(row["Last_Name"].ToString()))
                    Step.LastName = row["Last_Name"].ToString();
                if (!string.IsNullOrEmpty(row["Child_ID"].ToString()))
                    Step.ChildID = row["Child_ID"].ToString();
                if (!string.IsNullOrEmpty(row["Child_Number"].ToString()))
                    Step.ChildNumber = int.Parse(row["Child_Number"].ToString());
                if (!string.IsNullOrEmpty(row["Step"].ToString()))
                    Step.Step = row["Step"].ToString();
                if (!string.IsNullOrEmpty(row["Workflow_Code_ID"].ToString()))
                    Step.WorkflowID = int.Parse(row["Workflow_Code_ID"].ToString());
                if (!string.IsNullOrEmpty(row["Step_Code_ID"].ToString()))
                    Step.StepCodeID = int.Parse(row["Step_Code_ID"].ToString());
                if (!string.IsNullOrEmpty(row["File_ID"].ToString()))
                    Step.FileID = int.Parse(row["File_ID"].ToString());
                if (!string.IsNullOrEmpty(row["Child_Major_Life_Event_ID"].ToString()))
                    Step.MLEID = int.Parse(row["Child_Major_Life_Event_ID"].ToString());
                if (!string.IsNullOrEmpty(row["Child_Major_Life_Event_Lang_ID"].ToString()))
                    Step.MLELangID = int.Parse(row["Child_Major_Life_Event_Lang_ID"].ToString());
                if (!string.IsNullOrEmpty(row["Language_Code"].ToString()))
                    Step.LangCode = int.Parse(row["Language_Code"].ToString());
                if (!string.IsNullOrEmpty(row["Description"].ToString()))
                    Step.Description = row["Description"].ToString();
                if (!string.IsNullOrEmpty(row["stream_id"].ToString()))
                    Step.StreamID = row["stream_id"].ToString();
                TranslationSteps.Add(Step);
            }

            return TranslationSteps;
        }

        public DataSet GetDeclineList(int WorkFlowID, int ChildNum, string ChildID, bool PendingOnly, string UserID, bool QCApprover, int CountryID, int LocationID)
        {
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataSet OpenWorkFlows = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Child_Workflow_Decline_Summary]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                if (ChildNum > 0)
                    cmd.Parameters.AddWithValue("@Child_Number", ChildNum);
                if (!string.IsNullOrEmpty(ChildID))
                    cmd.Parameters.AddWithValue("@Child_ID", ChildID);
                if (WorkFlowID > 0)
                    cmd.Parameters.AddWithValue("@Workflow_Code_ID", WorkFlowID);
                cmd.Parameters.AddWithValue("@Pending_Only", PendingOnly);
                cmd.Parameters.AddWithValue("@QC_Approve", QCApprover);
                if (CountryID > 0)
                    cmd.Parameters.AddWithValue("@Country_Code_ID", CountryID);
                if (LocationID > 0)
                    cmd.Parameters.AddWithValue("@Location_Code_ID", LocationID);

                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(OpenWorkFlows);
            }
            catch (Exception ex)
            {
                DataTable table = new DataTable("Errors");
                table.Columns.Add("Message");
                table.Rows.Add(table.NewRow());
                table.Rows[0][0] = "ERROR: " + ex.Message;
                OpenWorkFlows.Tables.Add(table);
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            return OpenWorkFlows;
        }

        public List<DeclineDetails> GetChildDecline(string ChildID, int ChildNumber, bool PendingOnly, string UserID, bool QCApprover)
        {
            List<DeclineDetails> TranslationSteps = new List<DeclineDetails>();
            SqlConnection Con = new SqlConnection(sqlConnection);
            DataTable possibledup = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand("[dbo].[usp_Get_Child_Workflow_Decline_Detail]", Con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@User_ID", UserID);
                if (ChildNumber > 0)
                    cmd.Parameters.AddWithValue("@Child_Number", ChildNumber);
                if (!string.IsNullOrEmpty(ChildID))
                    cmd.Parameters.AddWithValue("@Child_ID", ChildID);
                cmd.Parameters.AddWithValue("@Pending_Only", PendingOnly);
                cmd.Parameters.AddWithValue("@QC_Approve", QCApprover);
                SqlDataAdapter adptr = new SqlDataAdapter();
                adptr.SelectCommand = cmd;
                Con.Open();
                adptr.Fill(possibledup);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Con.Close();
                Con.Dispose();
            }

            if (possibledup.Rows.Count > 0)
            {
                foreach (DataRow row in possibledup.Rows)
                {
                    DeclineDetails Step = new DeclineDetails();
                    if (!string.IsNullOrEmpty(row["Child_ID"].ToString()))
                        Step.ChildID = row["Child_ID"].ToString();
                    if (!string.IsNullOrEmpty(row["Child_Number"].ToString()))
                        Step.ChildNumber = int.Parse(row["Child_Number"].ToString());
                    if (!string.IsNullOrEmpty(row["Pending_Child_ID"].ToString()))
                        Step.PendingChildID = int.Parse(row["Pending_Child_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["ID"].ToString()))
                        Step.ID = int.Parse(row["ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Workflow"].ToString()))
                        Step.Workflow = row["Workflow"].ToString();
                    if (!string.IsNullOrEmpty(row["Step"].ToString()))
                        Step.Step = row["Step"].ToString();
                    if (!string.IsNullOrEmpty(row["File_ID"].ToString()))
                        Step.FileID = int.Parse(row["File_ID"].ToString());
                    if (!string.IsNullOrEmpty(row["Decline_Reason"].ToString()))
                        Step.DeclineReason = row["Decline_Reason"].ToString();
                    if (!string.IsNullOrEmpty(row["Decline_SubReason"].ToString()))
                        Step.DeclineSubReason = row["Decline_SubReason"].ToString();
                    if (!string.IsNullOrEmpty(row["Decline_Notes"].ToString()))
                        Step.DeclineNotes = row["Decline_Notes"].ToString();
                    if (!string.IsNullOrEmpty(row["Child_Workflow_Step_ID"].ToString()))
                        Step.ChildWorkflowStepID = int.Parse(row["Child_Workflow_Step_ID"].ToString());
                    TranslationSteps.Add(Step);
                }
            }

            return TranslationSteps;
        }

        public ReturnMessage SaveTranslatedMLE(string UserID, string ChildID, int LanguageCodeID, string MajorLifeDescription, int ChildMajorLifeEventID, int ChildMajorLifeEventLangID)
        {
            SqlConnection Con = new SqlConnection(sqlConnection);
            ReturnMessage ret = new ReturnMessage("1", "Success");

            try
            {
                SqlCommand cmnd = new SqlCommand("[Pending].[usp_Add_Child_Major_Life_Event_Other_Lang]", Con);
                cmnd.CommandType = CommandType.StoredProcedure;
                cmnd.Parameters.AddWithValue("@User_ID", UserID);
                cmnd.Parameters.AddWithValue("@Child_ID", ChildID);
                cmnd.Parameters.AddWithValue("@Start_Date", DateTime.Now);
                cmnd.Parameters.AddWithValue("@Language_Code_ID", LanguageCodeID);
                cmnd.Parameters.AddWithValue("@Major_Life_Description", MajorLifeDescription);
                //cmnd.Parameters.AddWithValue("@End_Date", "");
                cmnd.Parameters.AddWithValue("@Child_Major_Life_Event", ChildMajorLifeEventID);
                //cmnd.Parameters.AddWithValue("@Child_Major_Life_Event_Lang_ID", ChildMajorLifeEventLangID);
                Con.Open();
                cmnd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ret = new ReturnMessage("-7", "ERROR", "UpdateSave failed: " + ex.Message);
            }
            finally
            {
                Con.Close();
            }

            return ret;
        }
    }
}